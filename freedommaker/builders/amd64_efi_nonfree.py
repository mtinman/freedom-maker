#
# This file is part of Freedom Maker.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Worker class to build universal AMD64/Intel images with non-free hardware using UEFI.
"""

import pathlib

from .. import library

from .amd64 import AMD64ImageBuilder

class AMD64EFINonFreeImageBuilder(AMD64ImageBuilder):
    """Image builder for universal AMD/Intel images using UEFI."""
    efi_architecture = 'amd64'
    partition_table_type = 'gpt'
    efi_filesystem_type = 'vfat'
    efi_size = '256mib'
    boot_loader = 'grub'
    grub_target = 'amd64-efi'
    free = False

    @classmethod      
    """Return the command line name of target for this builder"""
    def get_target_name(cls):
       return 'amd64-efi-nonfree'


    def install_boot_loader(cls, state):
        """Install the boot manager."""
        library.install_grub(state, target=cls.grub_target, is_efi=True)


# In typical UEFI systems, EFI boot manager is available and all the OS
# list themselves as boot manager configuration in NVRAM. In many SBCs,
# NVRAM may not be present. In our case, since we distribute disk
# images, we don't have other operating system to boot. So, install
# grub as the boot manager.
        library.run_in_chroot(state, ['mkdir', '-p', '/boot/efi/EFI/boot/'])
        library.run_in_chroot(state, [
            'cp', f'/boot/efi/EFI/debian/grub{cls.efi_architecture}.efi',
            f'/boot/efi/EFI/boot/boot{cls.efi_architecture}.efi'
        ])

